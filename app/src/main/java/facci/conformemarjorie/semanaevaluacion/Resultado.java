package facci.conformemarjorie.semanaevaluacion;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.Calendar;

public class Resultado extends AppCompatActivity {
    TextView resultado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
        resultado = (TextView) findViewById(R.id.textViewResultado);
        String semana = getIntent().getExtras().getString("semana");

        Calendar calendario = Calendar.getInstance();
        int semanaActual = calendario.get(Calendar.WEEK_OF_YEAR);
        Log.e("SEMANA ACTUAL", String.valueOf(semanaActual));



        if(Integer.valueOf(semana) == semanaActual){

        }else {
            resultado.setText("INTENTA NUEVAMENTE");
            resultado.setTextColor(Color.RED);

            //resultado.setTextAppearance(this, R.style.EstiloIncorrecto);
        }

    }
}
