package facci.conformemarjorie.semanaevaluacion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText semana;
    Button revisar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        semana = (EditText) findViewById(R.id.editTextSemana);
        revisar = (Button) findViewById(R.id.btnRevisar);

        revisar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(semana.getText().toString().equals("")) {
                    /*Toast mensaje = Toast.makeText(getApplicationContext(), "Introduce el numero
                    de la semana actual", Toast.LENGTH_LONG);
                    mensaje.show();*/
                    semana.setError("Introduce el numero de semana actual");

                }else{
                    Intent intent = new Intent(MainActivity.this, Resultado.class);
                    intent.putExtra("semana", semana.getText().toString());
                    Log.e("SEMANA", semana.getText().toString());
                    startActivity(intent);
                }
            }
        });
    }
}